import pandas as pd
import ijson


class ExtractJson:

    def __init__(self, filename):
        self.file = filename

    def json_extraction(self):
        # self.file = 'agent.json'
        with open(self.file, 'r') as f:
            # modification of the objects within json is needed...
            objects = ijson.items(f, 'items.item')
            columns = list(objects)
        df_agt = pd.DataFrame(columns)

        # modification of columns for specific values is needed...
        df_agt2 = df_agt[['hostName', 'instanceName', 'ipAddr', 'storageModel', 'storageSerialNumber']]
        print(df_agt2.head())
        return df_agt2



