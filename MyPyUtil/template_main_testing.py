from MyPySSH import MyPySSH
import pandas as pd
from io import StringIO
from MyPySSH import MyPySSHTerminal

'''
# ssh terminal example
sshUsername = "graham"
sshPassword = "@p91em4c"
sshServer = "192.168.1.6"

connection = MyPySSHTerminal(sshServer, sshUsername, sshPassword)
connection.open_shell()
while True:
    command = input('$ ')
    if command.startswith(" "):
        command = command[1:]
    connection.send_shell(command)
'''


connection = MyPySSH("192.168.1.6", "graham", "@p91em4c")
# connection.send_command('df')
# connection.send_command('cd /mnt/Multimedia')
# connection.send_command('cd /mnt/Multimedia && ls -l')

cmddf = connection.output_command('df -h')
# print(cmddf)
# print('*************')
# print(type(cmddf))

# print('****NEXT LINE****')
# cmddf_dec = cmddf.decode('UTF-8')
# print(cmddf_dec)

print('****NEXT LINE****')
# testdat = StringIO(cmddf)
# df = pd.read_csv(testdat, delim_whitespace=True)
df = pd.read_csv(StringIO(cmddf), delim_whitespace=True, header=0)
# print(df)

print('****NEXT LINE****')
print(df[0:][['Size', 'Used', 'Avail']])
# df1 = df[1:][['Used', 'use%', 'Available']]
# print(df1)


# df_list = df[['Used', 'Available']]
# print(df_list)

