import pymysql


class MysqlConnector:

    def __init__(self, host, database, userid, passwd):
        self.hostname = host
        self.db = database
        self.user_id = userid
        self.pwd = passwd

    def get_connection(self):
        connection = pymysql.connect(host=self.hostname,
                                     user=self.user_id,
                                     password=self.pwd,
                                     db=self.db,
                                     charset='utf8mb4',
                                     cursorclass=pymysql.cursors.DictCursor)
        return connection

    def get_sql_data(self, query):
        sql_connection = self.get_connection()
        sql_query = query
        try:
            with sql_connection.cursor() as cursor:
                cursor.execute(sql_query)
                result = cursor.fetchone()
                while result:
                    return result
        finally:
            sql_connection.close()

