
import pymysql.cursors
import logging



def get_connection():
    import logging
    logger = logging.getLogger(__name__)

    logger.debug("module loaded")

    logging.debug('Connecting to database')

    connection = pymysql.connect(host='localhost',
                             user='graham',
                             password='@p91em4c',
                             db='CBDB',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)

    return connection

def chk_file(file1, file2):
    import os.path
    status = True

    for filename in [file1, file2]:
        if not os.path.exists(filename):
            if filename == file1:
                status = False
                print('Error - File: ', filename, ' does not exist, please confirm and start program again...')
            elif filename == file2:
                #status = False
                print('Error - File: ', filename, ' does not exist, Creating output file...')
                open(file2, 'w')
                print('Error - File: ', file2, ' has been created...\n \tContinuing script...')
        else:
            print(filename, ' - ready for processing')
    return status

def line_ref_subsys(subsys):

    if subsys == 'WDC_VSP_01':
        return '55841'
    elif subsys == 'WDC_VSP_11':
        return '51206'
    elif subsys == 'WDC_VSP_03':
        return '55953'


def line_ref_ldev(hex_ldev):

    '''
    if hex_ldev == '':
        print("************************************************no value found")
    else:
        dec_ldev = int(hex_ldev.replace(':', '').strip('"'), base=16)
        return dec_ldev
    '''
    if hex_ldev is not '':
        dec_ldev = int(hex_ldev.replace(':', '').strip('"'), base=16)
        return dec_ldev



def loop_run(file1, file2, today_date):
    with open(file1) as input_file, open(file2, 'w') as output_file:
        output_file.write(input_file.readline())
        for line in input_file:
            striped_line = line.strip().split(",")
            sub_serial = line_ref_subsys(striped_line[-2])
            dec_ldev = line_ref_ldev(striped_line[-1])

            loop_connection = get_connection()
            loop_mysql_query = (
                "SELECT `ldev_cap`, `ldev_usdcap` FROM `Ldevs` WHERE `sub_serial`=%s "
                "AND `ldev_id`=%s AND `data_date`=%s")

            try:
                with loop_connection.cursor() as cursor:
                    cursor.execute(loop_mysql_query, (sub_serial, dec_ldev, today_date))
                    loop_result = cursor.fetchone()
                    while loop_result:
                        return loop_result
            finally:
                loop_connection.close()

def err_loging():

    import logging

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)
    # create a file handler
    handler = logging.FileHandler('VmExtract36.log')
    handler.setLevel(logging.INFO)
    # create a logging format
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    # add the handlers to the logger
    logger.addHandler(handler)
    return logger
